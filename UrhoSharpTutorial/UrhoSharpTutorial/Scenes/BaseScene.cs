﻿using Urho;

namespace UrhoSharpTutorial.Scenes
{
    public class BaseScene
    {
        private int width;
        private int height;
        public Scene Scene;
        public Node CameraNode { get; set; }

        public BaseScene(int width, int height)
        {
            this.width = width;
            this.height = height;
        }

        public void InitScene()
        {
            Scene = new Scene();
            Scene.CreateComponent<Octree>();
        }

        public void CreateCamera(Vector3 vector3)
        {
            CameraNode = Scene.CreateChild("Camera");
            CameraNode.Position = vector3;
            Camera camera = CameraNode.CreateComponent<Camera>();
            camera.Orthographic = true;

            camera.OrthoSize = (float)Application.Current.Graphics.Height * Application.PixelSize;
        }

        public void SetupViewport()
        {
            var renderer = Application.Current.Renderer;
            Viewport vp = new Viewport(Application.Current.Context, Scene, CameraNode.GetComponent<Camera>());
            renderer.SetViewport(0, vp);
            vp.SetClearColor(Color.White);
        }

        public virtual void OnUpdate(float timeStep)
        {
        }

        public virtual void Destroy()
        {
        }
    }
}
