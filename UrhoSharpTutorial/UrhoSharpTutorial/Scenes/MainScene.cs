﻿using Urho;
using Urho.Actions;
using UrhoSharpTutorial.Helpers;

namespace UrhoSharpTutorial.Scenes
{
    public class MainScene : BaseScene
    {
        private MainSceneInput _mainInput;
        public float movementSize = 0.8f;
        public float movementSpeed = 0.25f;

        private bool isFaded = false;

        public MainScene(int width, int height) : base(width, height)
        {
            CreateScene();
        }

        private void CreateScene()
        {
            InitScene();
            CreateCamera(new Vector3(0f, 0f, 0f));
            AssetManager.LoadAssets(Application.Current.ResourceCache);
            LoadObjects();

            _mainInput = new MainSceneInput(this);
            _mainInput.OnEvaluateNode += MainInput_OnEvaluateNode;

            SetupViewport();
            CreateEvents();
        }

        private void LoadObjects()
        {
            ComponentManager.AddXETile(Scene, "Xamarin Expert", 0, 0);
        }

        private void MainInput_OnEvaluateNode(object sender, Node node)
        {
            node.RunActions(new FadeTo(1f, isFaded ? 1f : 0.1f));
            isFaded = !isFaded;
        }

        private void CreateEvents()
        {
            Application.Current.Input.TouchBegin += _mainInput.Input_TouchBegin;
            Application.Current.Input.TouchMove += _mainInput.Input_TouchMove;
            Application.Current.Input.TouchEnd += _mainInput.Input_TouchEnd;
            Application.Current.Input.KeyUp += _mainInput.Input_KeyUp;
        }
        
        public override void Destroy()
        {
            base.Destroy();
            Application.Current.Input.TouchBegin -= _mainInput.Input_TouchBegin;
            Application.Current.Input.TouchMove -= _mainInput.Input_TouchMove;
            Application.Current.Input.TouchEnd -= _mainInput.Input_TouchEnd;
            Application.Current.Input.KeyUp -= _mainInput.Input_KeyUp;
        }
    }
}