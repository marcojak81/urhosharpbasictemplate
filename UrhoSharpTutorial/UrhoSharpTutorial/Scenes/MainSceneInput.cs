﻿using System;
using Urho;
using Urho.Actions;

namespace UrhoSharpTutorial.Scenes
{
    internal class MainSceneInput
    {
        private readonly MainScene _scene;

        public event EventHandler<Node> OnEvaluateNode;

        public MainSceneInput(MainScene scene)
        {
            _scene = scene;
        }

        private Node GetSelectedNode(float objX, float objY)
        {
            Ray cameraRay = _scene.CameraNode.GetComponent<Camera>().GetScreenRay(objX / Application.Current.Graphics.Width, objY / Application.Current.Graphics.Height);
            var result = _scene.Scene.GetComponent<Octree>().RaycastSingle(cameraRay, RayQueryLevel.Triangle, 100);
            return result?.Node;
        }

        public void Input_TouchBegin(TouchBeginEventArgs obj)
        {
            Node selectedNode = GetSelectedNode((float)obj.X, (float)obj.Y);
            OnEvaluateNode?.Invoke(this, selectedNode);
        }

        public void Input_TouchMove(TouchMoveEventArgs obj)
        {
            Node selectedNode = GetSelectedNode((float)obj.X, (float)obj.Y);
            OnEvaluateNode?.Invoke(this, selectedNode);
        }

        public async void Input_TouchEnd(TouchEndEventArgs obj)
        {

        }

        internal void Input_KeyUp(KeyUpEventArgs obj)
        {
            if (obj.Key == Key.I)
            {
                var camera = _scene.CameraNode.GetComponent<Camera>();
                camera.Zoom += 0.05f;
            }
            else if (obj.Key == Key.O)
            {
                var camera = _scene.CameraNode.GetComponent<Camera>();
                camera.Zoom -= 0.05f;
            }
            else if (obj.Key == Key.W)
            {
                _scene.CameraNode.RunActions(new MoveTo(_scene.movementSpeed, new Vector3(_scene.CameraNode.Position2D.X, _scene.CameraNode.Position2D.Y + _scene.movementSize, -1.0f)));
            }
            else if (obj.Key == Key.S)
            {
                _scene.CameraNode.RunActions(new MoveTo(_scene.movementSpeed, new Vector3(_scene.CameraNode.Position2D.X, _scene.CameraNode.Position2D.Y - _scene.movementSize, -1.0f)));
            }
            else if (obj.Key == Key.A)
            {
                _scene.CameraNode.RunActions(new MoveTo(_scene.movementSpeed, new Vector3(_scene.CameraNode.Position2D.X - _scene.movementSize, _scene.CameraNode.Position2D.Y, -1.0f)));
            }
            else if (obj.Key == Key.D)
            {
                _scene.CameraNode.RunActions(new MoveTo(_scene.movementSpeed, new Vector3(_scene.CameraNode.Position2D.X + _scene.movementSize, _scene.CameraNode.Position2D.Y, -1.0f)));
            }
        }
    }
}
