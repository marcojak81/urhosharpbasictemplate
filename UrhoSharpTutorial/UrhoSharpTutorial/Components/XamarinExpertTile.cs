﻿using Urho;
using Urho.Urho2D;

namespace UrhoSharpTutorial.Components
{
    class XamarinExpertTile : StaticSprite2D
    {
        public int X, Y;

        public void Init(Sprite2D sprite, int x, int y, int z = 0)
        {
            SetXYPosition(x, y);
            BlendMode = BlendMode.Alpha;
            Sprite = sprite;
            Layer = 1;
            Node.Position = new Vector3(x, y, z);
        }

        public override void OnAttachedToNode(Node node)
        {
            base.OnAttachedToNode(node);
            //Node.RunActionsAsync(new FadeIn(1));
            //node.SetScale(1.35f);
        }

        internal void SetXYPosition(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}
