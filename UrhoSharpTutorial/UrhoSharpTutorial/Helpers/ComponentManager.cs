﻿using System;
using System.Collections.Generic;
using System.Text;
using Urho;
using UrhoSharpTutorial.Components;

namespace UrhoSharpTutorial.Helpers
{
    public static class ComponentManager
    {
        public static void AddXETile(Scene scene, string name, int x, int y)
        {
            Node spriteNode = scene.CreateChild(name);
            XamarinExpertTile staticSprite = spriteNode.CreateComponent<XamarinExpertTile>();
            staticSprite.Init(AssetManager.XamarinExpertSprite, x, y);
        }
    }
}