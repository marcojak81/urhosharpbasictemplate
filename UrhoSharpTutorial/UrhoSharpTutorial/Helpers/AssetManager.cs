﻿using System;
using System.Collections.Generic;
using System.Text;
using Urho.Resources;
using Urho.Urho2D;

namespace UrhoSharpTutorial.Helpers
{
    internal class AssetManager
    {
        public static Sprite2D XamarinExpertSprite;

        public static void LoadAssets(ResourceCache cache)
        {
            XamarinExpertSprite = cache.GetSprite2D(Assets.XamarinExpertSprite);
        }
    }
}
