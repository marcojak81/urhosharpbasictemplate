﻿using Xamarin.Forms;

namespace UrhoSharpTutorial
{
    public partial class MainPage : ContentPage
    {
        XamarinExpertApp urhoApp;

        public MainPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            StartUrhoApp();
        }

        async void StartUrhoApp()
        {
            urhoApp = await urhoSurface.Show<XamarinExpertApp>(new Urho.ApplicationOptions("Data") { Orientation = Urho.ApplicationOptions.OrientationType.LandscapeAndPortrait });
        }
    }
}
