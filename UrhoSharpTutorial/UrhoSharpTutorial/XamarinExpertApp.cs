﻿using System.Diagnostics;
using Urho;
using UrhoSharpTutorial.Scenes;

namespace UrhoSharpTutorial
{
    public class XamarinExpertApp : Application
    {
        private BaseScene _mainScene;

        [Preserve]
        public XamarinExpertApp(ApplicationOptions options = null) : base(options)
        {
        }

        static XamarinExpertApp()
        {
            UnhandledException += (s, e) =>
            {
                if (Debugger.IsAttached)
                    Debugger.Break();
                e.Handled = true;
            };
        }

        protected override void Start()
        {
            base.Start();
            Input.Enabled = true;
            Input.SetMouseVisible(true, false);
            Input.TouchEmulation = true;

            StartMainScene();
        }

        private void StartMainScene()
        {
            _mainScene?.Destroy();
            Current.UI.Root.RemoveAllChildren();
            _mainScene = new MainScene(Graphics.Width, Graphics.Height);
        }

        protected override void OnUpdate(float timeStep)
        {
            base.OnUpdate(timeStep);
            _mainScene.OnUpdate(timeStep);
        }
    }
}